// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyD_DA5hzmkigM5TFW9zntLUmUfNGiETikY",
    authDomain: "ex2019-9242c.firebaseapp.com",
    databaseURL: "https://ex2019-9242c.firebaseio.com",
    projectId: "ex2019-9242c",
    storageBucket: "ex2019-9242c.appspot.com",
    messagingSenderId: "511417142230",
    appId: "1:511417142230:web:0ca8365761faf7521df277"
  }  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
